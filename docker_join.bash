#!/usr/bin/env bash
#
# Typical usage: ./join.bash subt
#

IMG=argnctu/subt_reference_datasets_devel

xhost +
containerid=$(docker ps -aqf "ancestor=${IMG}")
docker exec --privileged -e DISPLAY=${DISPLAY} -e LINES=`tput lines` -it ${containerid} bash
xhost -
